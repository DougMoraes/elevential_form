# Desafio Elevential

# Ferramentas utilizadas e motivos

Jquery - Utilizado para excluir e manipular elementos na DOM - A exclus�o da box e a mudan�a no texto em "Show/Hide Password" foi feita com Jquery pela praticidade na programa��o, ainda que pudesse ser feito usando apenas javascript.

Materialize - Biblioteca utilizada para adequar o estilo do formul�rio. O padr�o do formul�rio passado pelo designer j� era muito parecido ao utilizado na biblioteca.

Toastr - (https://codeseven.github.io/toastr/) Utilizo o toastr para melhorar o visual dos alertas. Acredito que as mensagens via toast s�o menos invasivas e d�o o mesmo tom de alertas.
 
## Sobre o Desafio

O desafio foi bastante interessante, gosto particularmente de processos seletivos nos quais eu j� aprenda alguma coisa, acredito que esse modo n�o seja muito custoso para a empresa e para o candidato j� � uma forma de adquirir conhecimento mesmo sem conseguir a vaga. Esse em espec�fico foi bacana por j� me apresentar a estrutura do que era esperado, limitar o tamanho da box foi �timo pois me obrigou a trabalhar no espa�amento entre os itens do forms.

## Dificuldades

Alinhamentos. para mim � sempre trabalhoso lidar com o posicionamento dos itens. Preciso praticar mais e acredito que com o tempo irei aprimorar mais esse campo. O bot�o Next foi particularmente dif�cil de posicionar. Optei por trabalhar com a margem no topo negativa j� que a margem na base n�o estava posicionando o bot�o. 

## Melhorias

Eu poderia fazer o posicionamento do bot�o Next de um modo melhor.
Talvez o Jquery n�o seja a melhor op��o para o cliente, o uso dele nesse caso foi simples e agilizou bastante a codifica��o mas talvez n�o fosse realmente necess�rio.

## Prioridades

Julguei a valida��o do formul�rio a parte mais importante por entender que essa era a principal fun��o do projeto (criar uma m�todo de entrada para informa��es e verificar sua consist�ncia seguindo um conjunto de regras). No entanto optei por desenvolver todo o layout primeiro visto que eu sabia que teria dificuldades com posicionamentos, acredito que desse modo eu teria um maior controle de tempo, verificando se ainda existiria tempo h�bil para a finaliza��o do projeto.