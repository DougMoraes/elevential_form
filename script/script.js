// Write your JavaScript here 

$(document).ready(function(){

	var button_next = $('#button_next');
	var login = $('#login') 
	var hide_password = $('.hide_password:first');
	var hide_password_confirmation = $('.hide_password:last');
	var user_name = $('#user_name');
	var user_email = $('#user_email')
	var user_password = $('#user_password');
	var user_password_confirm = $('#user_password_confirm');
	var close_form = $('#close_form');
	var box_form = $('#box_form');
	var button_create = $('#button_create');

	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-top-full-width",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut",
	};

	button_create.hide();

    hide_password.click(function(){		
    	var str = hide_password.text();

		console.log(str);

		if (str === 'Show password'){
			hide_password.text('Hide password');
			user_password.prop('type', 'text');
		} else {
			hide_password.text('Show password');
			user_password.prop('type', 'password');
		};
	});

	hide_password_confirmation.click(function(){		
    	var str = hide_password_confirmation.text();

		console.log(str);

		if (str === 'Show password'){
			hide_password_confirmation.text('Hide password');
			user_password_confirm.prop('type', 'text');
		} else {
			hide_password_confirmation.text('Show password');
			user_password_confirm.prop('type', 'password');
		};
	});

	button_next.click(function(){
		console.log('clicou');

        var allFilled = true;
        var passConfirmed = true;

        $("input").each(function() {
        	console.log('validando');

            var element = $(this);

            if (element.val() == ""){ 
            	allFilled = false;
            	console.log('Algo está vazio');
            }
        });

        if (user_password.val() != user_password_confirm.val()){
        	passConfirmed = false;
        };

        if(allFilled == false){ 
        	Command: toastr["error"]("All fields must be filleds", "Error!");
        	console.log('Faltou preencher coisa');
        	return false;
        } else if (passConfirmed == false) {
        	Command: toastr["error"]("Password and confirmation didn't match", "Error!");
        	console.log('campos diferentes');
        } else { 
        	Command: toastr["success"]("Now you can log in", "Success!");
        	console.log('Tudo ok');
        }	
	});

	close_form.click(function(){
		box_form.hide();
		button_create.show();
	});

	login.click(function(){
		Command: toastr["warning"]("Work in progress! This feature is not ready yet!", "WARNING!");
	})

	button_create.click(function(){
		box_form.show();
		button_create.hide();
	})
});

